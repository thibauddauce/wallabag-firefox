module Main where

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Ref (modifyRef', Ref, newRef)
import Data.Config (Action, Event, fromPreferences, addEventListenerForPreferences) as Config
import Data.State (State(State))
import Data.Entries (Event, init, addEventListenerForTabChange) as Entries
import Prelude (($), (<$>), (<*>), (>>>), pure, unit, Unit, bind)
import Sdk.SimplePrefs (PREFERENCES)
import Sdk.Tabs (TABS)

data Event = EntriesEvent Entries.Event | ConfigEvent Config.Event
data Action = ConfigAction Config.Action

init :: forall eff. Eff (preferences :: PREFERENCES, tabs :: TABS | eff) State
init = State <$> ({ entries: _
                 , config: _
                 }
             <$> Entries.init
             <*> Config.fromPreferences)

main :: Eff _ Unit
main = do
  initialState <- init
  state <- newRef initialState
  let unsafeUpdate = unsafeUpdate' state

  Entries.addEventListenerForTabChange (EntriesEvent >>> unsafeUpdate)
  Config.addEventListenerForPreferences (ConfigEvent >>> unsafeUpdate)
  pure $ unit

unsafeUpdate' :: Ref State -> Event -> Eff _ Unit
unsafeUpdate' state event = do
  actions <- modifyRef' state (update event)
  pure $ unit

update :: Event -> State -> { state :: State, value :: Array Action }
update (EntriesEvent event) state = { state: state, value: []}
update (ConfigEvent event) state = { state: state, value: []}
