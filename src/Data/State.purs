module Data.State where

import Data.Config (Event, Config) as Config
import Data.Entries (Entries)

newtype State = State { entries :: Entries
                      , config :: Config.Config
                      }

data Event = ConfigEvent Config.Event
