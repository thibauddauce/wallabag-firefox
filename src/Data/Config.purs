module Data.Config where

import Control.Monad.Eff (Eff)
import Control.Monad.Eff.Console (CONSOLE, log)
import Data.Maybe (Maybe(Just, Nothing))
import Data.String (toChar)
import Data.Url (parse, fromMaybe, Url(..)) as Url
import Prelude (show, Unit, ($), (<$>), (<*>), bind, (>>>))
import Sdk.SimplePrefs (setPreference, onChange, getMaybePreference, PREFERENCES)

newtype BaseUrl = BaseUrl Url.Url
data ClientId = NoClientId |ClientId String
data SecretKey = NoSecretKey | SecretKey String
newtype ShortcutKey = ShortcutKey Char

newtype Config = Config { baseUrl :: BaseUrl
                        , clientId :: ClientId
                        , secretKey :: SecretKey
                        , shortcutKey :: ShortcutKey
                        }

data Event = BaseUrlHasBeenChanged BaseUrl
           | ClientIdHasBeenChanged ClientId
           | SecretKeyHasBeenChanged SecretKey
           | ShortcutKeyHasBeenChanged ShortcutKey
           | ApiClientHasBeenChanged ClientId SecretKey

data Action = ChangeBaseUrl BaseUrl
            | ChangeClientId ClientId
            | ChangeSecretKey SecretKey
            | ChangeShortcutKey ShortcutKey

parseClientId :: String -> ClientId
parseClientId "" = NoClientId
parseClientId clientId = ClientId clientId

clientIdFromMaybe :: Maybe String -> ClientId
clientIdFromMaybe Nothing = NoClientId
clientIdFromMaybe (Just clientId) = parseClientId clientId

parseSecretKey :: String -> SecretKey
parseSecretKey "" = NoSecretKey
parseSecretKey secretKey = SecretKey secretKey

secretKeyFromMaybe :: Maybe String -> SecretKey
secretKeyFromMaybe Nothing = NoSecretKey
secretKeyFromMaybe (Just "") = NoSecretKey
secretKeyFromMaybe (Just secretKey) = SecretKey secretKey

defaultShortcutKey :: Char
defaultShortcutKey = 's'

parseShortcutKey :: String -> ShortcutKey
parseShortcutKey shortcutKeyString = case toChar shortcutKeyString of
  Nothing -> ShortcutKey defaultShortcutKey
  Just shortcutKey -> ShortcutKey shortcutKey

shortcutKeyFromMaybe :: Maybe String -> ShortcutKey
shortcutKeyFromMaybe Nothing = ShortcutKey defaultShortcutKey
shortcutKeyFromMaybe (Just shortcutKeyString) = parseShortcutKey shortcutKeyString

fromPreferences :: forall eff. Eff (preferences :: PREFERENCES | eff) Config
fromPreferences = Config <$> ({ baseUrl: _, clientId: _, secretKey: _, shortcutKey: _}
  <$> (BaseUrl <$> Url.fromMaybe <$> getMaybePreference "wallabagUrl")
  <*> (clientIdFromMaybe <$> getMaybePreference "wallabagClientId")
  <*> (secretKeyFromMaybe <$> getMaybePreference "wallabagSecretId")
  <*> (shortcutKeyFromMaybe <$> getMaybePreference "wallabagShortcutKey"))

addEventListenerForPreferences :: forall a b. (Event -> (Eff a Unit)) -> Eff (preferences :: PREFERENCES | b) Unit
addEventListenerForPreferences sendEvent = do
  onChange "wallabagUrl" (Url.parse >>> BaseUrl >>> BaseUrlHasBeenChanged >>> sendEvent)
  onChange "wallabagClientId" (parseClientId >>> ClientIdHasBeenChanged >>> sendEvent)
  onChange "wallabagSecretKey" (parseSecretKey >>> SecretKeyHasBeenChanged >>> sendEvent)
  onChange "wallabagShortcutKey" (parseShortcutKey >>> ShortcutKeyHasBeenChanged >>> sendEvent)

update :: Event -> Config -> Config
update event (Config config) = Config $ case event of
  BaseUrlHasBeenChanged baseUrl -> config { baseUrl = baseUrl }
  ClientIdHasBeenChanged clientId -> config { clientId = clientId }
  SecretKeyHasBeenChanged secretKey -> config { secretKey = secretKey }
  ShortcutKeyHasBeenChanged shortcutKey -> config { shortcutKey = shortcutKey }
  ApiClientHasBeenChanged clientId secretKey -> config { clientId = clientId, secretKey = secretKey }

action :: Event -> Array Action
action (BaseUrlHasBeenChanged baseUrl) = [ChangeBaseUrl baseUrl]
action (ClientIdHasBeenChanged clientId) = [ChangeClientId clientId]
action (SecretKeyHasBeenChanged secretKey) = [ChangeSecretKey secretKey]
action (ShortcutKeyHasBeenChanged shortcutKey) = [ChangeShortcutKey shortcutKey]
action (ApiClientHasBeenChanged clientId secretKey) = [ChangeClientId clientId, ChangeSecretKey secretKey]

effect :: forall eff. Action -> (Eff (console :: CONSOLE, preferences :: PREFERENCES | eff) Unit)
effect (ChangeBaseUrl (BaseUrl Url.InvalidUrl)) = log "Trying to change the base URL with an invalid URL"
effect (ChangeBaseUrl (BaseUrl (Url.Url baseUrl))) = setPreference "wallabagUrl" baseUrl
effect (ChangeClientId NoClientId) = log "Trying to change the client ID with an invalid string"
effect (ChangeClientId (ClientId clientId)) = setPreference "wallabagClientId" clientId
effect (ChangeSecretKey NoSecretKey) = log "Trying to change the secret key with an invalid string"
effect (ChangeSecretKey (SecretKey secretKey)) = setPreference "wallabagSecretKey" secretKey
effect (ChangeShortcutKey (ShortcutKey char)) = setPreference "wallabagShortcutKey" $ show char
