module Data.Synchronisable where

data Synchronisable a = Nothing | Sending a | Sent a | NeedAsking | Asked
