module Data.Url where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe(Just, Nothing))
import Prelude (Unit, (<$>), (>>>))
import Sdk.Tabs (onTabChange, getCurrentTabUrl, TABS)

data Url = InvalidUrl | Url String

parse :: String -> Url
parse "" = InvalidUrl
parse url = Url url

fromMaybe :: Maybe String -> Url
fromMaybe Nothing = InvalidUrl
fromMaybe (Just url) = parse url

fromTab :: forall eff. Eff (tabs :: TABS | eff) Url
fromTab = parse <$> getCurrentTabUrl
