module Data.Remote where

import Data.Synchronisable (Synchronisable)

data Remote a = Local (Synchronisable a) a | Remote (Synchronisable a) a
