module Data.Entries where

import Control.Monad.Eff (Eff)
import Data.Map (pop, insert, empty, Map)
import Data.Maybe (Maybe(Just, Nothing))
import Data.Remote (Remote(Local))
import Data.Synchronisable (Synchronisable(Nothing)) as S
import Data.Tuple (Tuple(Tuple))
import Data.Url (Url(InvalidUrl, Url), fromTab, parse) as Url
import Prelude (id, Unit, ($), (<$>), (>>>))
import Sdk.Tabs (onTabChange, TABS)

data Status = Saved | Unsaved
newtype Entry = Entry { status :: Remote Status
                      , url :: Url.Url
                      }

newtype Entries = Entries { current :: Entry
                          , others :: Map String Entry
                          }

data Event = UrlHasChanged Url.Url

newEntry :: Url.Url -> Entry
newEntry = Entry <$> {status: Local S.Nothing Unsaved, url: _}

getUrl :: Entry -> Url.Url
getUrl (Entry entry) = entry.url

init :: forall eff. Eff (tabs :: TABS | eff) Entries
init = Entries <$> ({ current: _, others: empty :: Map String Entry } <$> fromTab)

fromTab :: forall eff. Eff (tabs :: TABS | eff) Entry
fromTab = newEntry <$> Url.fromTab

addEventListenerForTabChange :: forall a b. (Event -> (Eff a Unit)) -> Eff (tabs :: TABS | b) Unit
addEventListenerForTabChange sendEvent = onTabChange (Url.parse >>> UrlHasChanged >>> sendEvent)

update :: Event -> Entries -> Entries
update event (Entries entries) = Entries $ case event of
  UrlHasChanged url -> entries { current = newEntry url}

toggleUrl :: Url.Url -> Entries -> Entries
toggleUrl newUrl (Entries entries) = case newUrl of
  Url.InvalidUrl -> case currentUrl of
    Url.InvalidUrl -> (Entries entries)
    Url.Url oldStringUrl -> Entries $ entries { current = newEntry newUrl
                                              , others = insert oldStringUrl entries.current entries.others
                                              }
  Url.Url newStringUrl -> case currentUrl of
    Url.InvalidUrl -> Entries $ case pop newStringUrl entries.others of
      Nothing -> entries { current = newEntry newUrl }
      Just (Tuple saved otherEntries) -> entries { current = saved, others = otherEntries }
    Url.Url oldStringUrl -> Entries $ case pop newStringUrl entries.others of
      Nothing -> entries { current = newEntry newUrl }
      Just (Tuple saved otherEntries) -> entries { current = saved, others = otherEntries }
  where
  currentUrl = getUrl entries.current
