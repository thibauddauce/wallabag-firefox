module Sdk.SimplePrefs where

import Control.Monad (map)
import Control.Monad.Eff (Eff)
import Data.Maybe (fromMaybe, Maybe(Just, Nothing))
import Prelude (Unit)

foreign import data PREFERENCES :: !
foreign import getPreferenceImpl :: forall eff m. String -> m -> (String -> m) -> (Eff (preferences :: PREFERENCES | eff) m)
foreign import setPreference :: forall eff. String -> String -> (Eff (preferences :: PREFERENCES | eff) Unit)
foreign import onChange :: forall eff all. String -> (String -> Eff all Unit) -> (Eff (preferences :: PREFERENCES | eff) Unit)

getMaybePreference :: forall eff. String -> (Eff (preferences :: PREFERENCES | eff) (Maybe String))
getMaybePreference key = getPreferenceImpl key Nothing Just

getPreference :: forall eff. String -> String -> (Eff (preferences :: PREFERENCES | eff) String)
getPreference key default = map (fromMaybe default) (getMaybePreference key)
