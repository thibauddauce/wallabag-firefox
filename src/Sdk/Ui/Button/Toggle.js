'use strict'

const ToggleButton = require('sdk/ui/button/toggle').ToggleButton

exports.createImpl = function (options) {
  return function () {
    return ToggleButton(options)
  }
}

exports.onButtonClick = function (button) {
  return function (callback) {
    return function () {
      button.on('click', callback)
    }
  }
}

exports.uncheck = function (button) {
  return function () {
    button.state('window', {checked: false})
  }
}

exports.check = function (button) {
  return function () {
    button.state('window', {checked: true})
  }
}
