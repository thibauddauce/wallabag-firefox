module Sdk.Ui.Button.Toggle where

import Control.Monad.Eff (Eff)
import Data.Maybe (fromMaybe, Maybe)
import Prelude (pure, unit, Unit, ($))

foreign import data BUTTON :: !

type ToggleButtonOptions = { id :: String
                           , label :: String
                           , icon :: String
                           , disabled :: Maybe Boolean
                           , onChange :: forall eff. Maybe (Eff eff Unit)
                           , onClick :: forall eff. Maybe (Eff eff Unit)
                           , badge :: Maybe String
                           , badgeColor :: Maybe String
                           }

type AllButtonOptions = { id :: String
              , label :: String
              , icon :: String
              , disabled :: Boolean
              , onChange :: forall eff. (Eff eff Unit)
              , onClick :: forall eff. (Eff eff Unit)
              , badge :: String
              , badgeColor :: String
              }

data Button = Button

foreign import createImpl :: forall eff. AllButtonOptions -> (Eff (button :: BUTTON | eff) Button)
foreign import onButtonClick :: forall eff. Button -> (Eff (button :: BUTTON | eff) Unit) -> (Eff (button :: BUTTON | eff) Unit)
foreign import uncheck :: forall eff. Button -> (Eff (button :: BUTTON | eff) Unit)
foreign import check :: forall eff. Button -> (Eff (button :: BUTTON | eff) Unit)

create :: forall eff. ToggleButtonOptions -> (Eff (button :: BUTTON | eff) Button)
create options = createImpl { id: options.id
                            , label: options.label
                            , icon: options.icon
                            , disabled: (fromMaybe false options.disabled)
                            , onChange: (fromMaybe (pure $ unit) options.onChange)
                            , onClick: (fromMaybe (pure $ unit) options.onClick)
                            , badge: (fromMaybe "" options.badge)
                            , badgeColor: (fromMaybe "red" options.badgeColor)
                            }
