module Sdk.Panel where

import Control.Monad.Eff (Eff)
import Prelude (Unit)
import Sdk.Ui.Button.Toggle (Button)

type Panel = { isShowing :: Boolean }
type PanelShowOptions = { contentURL :: String
                        , width :: Int
                        , height :: Int
                        , position :: Button
                        }

foreign import data PANEL :: !
foreign import create :: forall eff. (Eff (panel :: PANEL | eff) Panel)
foreign import onPanelHide :: forall eff. Panel -> (Eff (panel :: PANEL | eff) Unit) -> (Eff (panel :: PANEL | eff) Unit)
foreign import onPanelShow :: forall eff. Panel -> (Eff (panel :: PANEL | eff) Unit) -> (Eff (panel :: PANEL | eff) Unit)
foreign import show :: forall eff. Panel -> PanelShowOptions -> (Eff (panel :: PANEL | eff) Unit)
foreign import onNewBaseUrl :: forall eff. Panel -> (String -> Eff (panel :: PANEL | eff) Unit) -> (Eff (panel :: PANEL | eff) Unit)
foreign import onNewApiClient :: forall eff. Panel -> (String -> String -> Eff (panel :: PANEL | eff) Unit) -> (Eff (panel :: PANEL | eff) Unit)
