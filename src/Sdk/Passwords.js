'use strict'

const passwords = require('sdk/passwords')
const self = require('sdk/self')

exports.getImpl = function (realm) {
  return function (callback) {
    return function (nothing) {
      return function (just) {
        return function () {
          passwords.search({
            url: self.uri,
            realm: realm,
            onComplete: function (credentials) {
              if (credentials.length === 0) {
                return callback(nothing)()
              } else {
                return callback(just(credentials[0].password))()
              }
            }
          })
        }
      }
    }
  }
}
