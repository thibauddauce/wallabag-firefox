module Passwords where

import Control.Monad.Eff (Eff)
import Data.Maybe (Maybe)
import Prelude (Unit, bind)
import Sdk.Passwords (get, PASSWORDS)

data PasswordsHaveBeenChanged = NewAccessToken (Maybe String) | NewRefreshToken (Maybe String)
type Passwords = { accessToken :: Maybe String
                 , refreshToken :: Maybe String
                 }

updatePasswords :: PasswordsHaveBeenChanged -> Passwords -> Passwords
updatePasswords (NewAccessToken accessToken) passwords =  passwords { accessToken = accessToken }
updatePasswords (NewRefreshToken refreshToken) passwords =  passwords { refreshToken = refreshToken }

fetchPasswords :: forall eff. (PasswordsHaveBeenChanged -> (Eff (passwords :: PASSWORDS | eff) Unit)) -> (Eff (passwords :: PASSWORDS | eff) Unit)
fetchPasswords updateBy = do
  get "access_token" (\newAccessToken -> updateBy (NewAccessToken newAccessToken))
  get "refresh_token" (\newRefreshToken -> updateBy (NewRefreshToken newRefreshToken))
