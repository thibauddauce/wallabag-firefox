# Wallabag Firefox extension

## Contributing

To contribute, you need Purescript, Pulp and JPM.

```bash
pulp build -O -t output/index.js
jpm xpi
jpm run -b /usr/bin/firefox
```

Next, visit `about:debugging` and click on "Load Temporary Add-on". Select your `wallabag.xpi` file.
